import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-outletlist',
  templateUrl: './outletlist.component.html',
  styleUrls: ['./outletlist.component.scss']
})
export class OutletlistComponent implements OnInit {
  public OtherVariables = {
    loading: true
  }
  public outletLists:any;
  constructor(
      private apiServ:ApiService,
      private router:Router
  ) { }

  ngOnInit() {
      this.getOutletDetails();
  }

  getOutletDetails() {
    this.OtherVariables.loading = true
    let converterParams = {
      METHOD: 'GET',
      URL: environment.apiEndPoint + '/GetAllOutlets',
      DATA: {
          BusinessUnitID: '',
          OutletID: ''
      }
    }  

    this.apiServ.getOutletDetails(converterParams).then((resp) => {
        
        if(resp && !resp.error && resp.hasOwnProperty('data') && resp.data.length) {
            this.outletLists = resp.data;
            console.log("RESPONSE",this.outletLists)
        }
        this.OtherVariables.loading = false;
    }).catch(err => {
        console.log("ERROR in Fertching details",err);
        this.OtherVariables.loading = false;
    });

  }

  rootPage() {
    console.log(111)
      // this.router.navigate(['']);
  }

}
