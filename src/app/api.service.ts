import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable, of } from 'rxjs';
import { environment } from '../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  _CallApi(args): Observable<any> {

    let URLParams = new HttpParams();
     if (args.hasOwnProperty("urlParams") && Object.keys(args.urlParams).length > 0) {
      Object.keys(args.urlParams).forEach(key => {
        URLParams = URLParams.append(key, args.urlParams[key]);
      });
    }
    let headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8', "Token":environment.Converter.token });
    /*let formData = new FormData;
    if (args.hasOwnProperty('body')) {
      Object.keys(args.body).forEach(key => {
        formData.append(key, args.body[key]);
      });
    } */
    let postData = '';
    if(args && args.hasOwnProperty('body')) {
        postData = args.body;
    }

    switch (args.method.toUpperCase()) {
      case "POST": {
        return this.http.post(environment.Converter.endpoint, postData, {
          params: URLParams,
          headers : headers
        });
      }
      case "SMS": {
        return this.http.post('https://api.casinoeatz.com/public/api/sendTsms', postData, {
          params: URLParams,
          headers : headers
        });
      }
      case "GET":
      default: {
        return this.http.get(environment.Converter.endpoint, {
          params: URLParams,
          headers : headers
        });
      }
    }
  }

  getOutletDetails(params:any='') {
      let apiParams = {
          method: "POST",
          body : params
      }
      return this._CallApi(apiParams).toPromise();
  }

  insertUpdateBooking(params:any='') {
      let apiParams = {
          method: "POST",
          body : params
      }
      return this._CallApi(apiParams).toPromise();
  }

  bookingAvailability(params:any='') {
      let apiParams = {
          method: "POST",
          body : params
      }
      return this._CallApi(apiParams).toPromise();
  }

  getBookingDetails(params:any='') {
      let apiParams = {
          method: "POST",
          body : params
      }
      return this._CallApi(apiParams).toPromise();
  }
  GetOutletSettings(params:any='') {
      let apiParams = {
          method: "POST",
          body : params
      }
      return this._CallApi(apiParams).toPromise();
  }

  sendsms(params:any='') {
    let apiParams = {
        method: "SMS",
        body : params
    }
    return this._CallApi(apiParams).toPromise();
}

}
