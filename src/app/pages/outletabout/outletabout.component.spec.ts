import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutletaboutComponent } from './outletabout.component';

describe('OutletaboutComponent', () => {
  let component: OutletaboutComponent;
  let fixture: ComponentFixture<OutletaboutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutletaboutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutletaboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
