import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-cancelbooking',
  templateUrl: './cancelbooking.component.html',
  styleUrls: ['./cancelbooking.component.scss']
})
export class CancelbookingComponent implements OnInit {
  public OtherVariables = {
    bookingID: null,
    routerOutletId: null,
    bookingDetails: null,
    loading: true,
    BookingStatusInArray: {
      0: 'pending',
      1: 'confirmed',
      2: 'cancelled ',
      3: 'closed '
    },
  }
  constructor(
    public _api: ApiService,
    public _router: Router,
    public _toster: ToastrService,
    public _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.OtherVariables.bookingID = this._activatedRoute.snapshot.paramMap.get('bookingID');
    this.getBookingDetails();
  }
  getBookingDetails() {
    this.OtherVariables.loading = true
    let converterParams = {
      METHOD: 'GET',
      URL: environment.apiEndPoint + 'GetBooking',
      DATA: {
        bookingid: this.OtherVariables.bookingID,
        outletid: 'null',
        pageno: 0,
        maximumrows: 1,
        usepaging: 'false'
      }
    }
    console.log("converterParams", converterParams);

    this._api.getBookingDetails(converterParams).then((resp) => {
      if (resp && !resp.error && resp.hasOwnProperty('data') && resp.data.length && resp.data[0]) {
        this.OtherVariables.bookingDetails = resp.data[0];
        console.log("RESPONSE", this.OtherVariables.bookingDetails)
      }
      this.OtherVariables.loading = false;
    }).catch(err => {
      this.OtherVariables.loading = false;
      console.log("ERROR in Fertching details", err)
    });

  }
  cancelBooking() {
    //if (confirm("Are you sure to cancel this reservation?")) {
      var d = new Date();
      var datestring = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes();
      let converterParams = {
        METHOD: 'POST',
        URL: environment.apiEndPoint + 'BookingStatusChange',
        DATA: {
          BookingID: this.OtherVariables.bookingID,
          Status: 2,
          CancelReason: "",
          CancelationDate: datestring
        }
      }
      this._api.getBookingDetails(converterParams).then((resp) => {
        if (resp.data.Successful) {
          this._toster.success("Your reservation successfully cancelled");
          this._router.navigate(["booking-details/" + this.OtherVariables.bookingID])
        }
      }).catch(err => {
        console.log("ERROR in Fertching details", err)
      });
    /*} else {
      this._router.navigate(["booking-details/" + this.OtherVariables.bookingID])
    }*/
  }

  gotoedit() {
    this._router.navigate(["booking-details/" + this.OtherVariables.bookingID])
  }

}
