import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule }    from '@angular/common/http';
import { AppComponent } from './app.component';
import { BookingComponent } from './pages/booking/booking.component';
// Forms
import { ReactiveFormsModule } from '@angular/forms';
import { OutletlistComponent } from './pages/outletlist/outletlist.component';
// Toast
import { ToastrModule } from 'ngx-toastr';
import { OutletaboutComponent } from './pages/outletabout/outletabout.component';
import { EditbookingComponent } from './pages/editbooking/editbooking.component';
import { BookingDetailsComponent } from './pages/booking-details/booking-details.component';
import { CancelbookingComponent } from './pages/cancelbooking/cancelbooking.component';
// DatePicker
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [
    AppComponent,
    BookingComponent,
    OutletlistComponent,
    OutletaboutComponent,
    EditbookingComponent,
    BookingDetailsComponent,
    CancelbookingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      tapToDismiss:true
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
