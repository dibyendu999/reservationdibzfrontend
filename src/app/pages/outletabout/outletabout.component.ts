import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ApiService } from '../../api.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-outletabout',
  templateUrl: './outletabout.component.html',
  styleUrls: ['./outletabout.component.scss']
})
export class OutletaboutComponent implements OnInit {

  private routerOutletId:any;
  public outletDetails:any;
  constructor(
      private callApi:ApiService,
      public toastr:ToastrService,
      private route:ActivatedRoute
  ) { 
    
      this.routerOutletId = this.route.snapshot.params.id;
      console.log("this.routerOutletId",this.routerOutletId)
  }

  ngOnInit() {
    this.getOutletDetails();
  }

  getOutletDetails() {

    let converterParams = {
        METHOD: 'GET',
        URL: environment.apiEndPoint + '/GetAllOutlets',
        DATA: {
            BusinessUnitID: '',
            OutletID: this.routerOutletId
        }
    }  

    this.callApi.getOutletDetails(converterParams).then((resp) => {
        if(resp && !resp.error && resp.hasOwnProperty('data') && resp.data.length && resp.data[0]) {
            this.outletDetails = resp.data[0];
            console.log("RESPONSE",this.outletDetails)
            // this.toastr.success('Hello world!', 'Toastr fun!');
        }
    }).catch(err => {
        this.toastr.error('Error in fetching outlet!', 'Error');
        console.log("ERROR in Fertching details",err)
    });

}

}
