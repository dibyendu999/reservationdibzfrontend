import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.scss']
})
export class BookingDetailsComponent implements OnInit {

  public OtherVariables = {
    bookingID: null,
    routerOutletId: null,
    bookingDetails: null,
    loading: true,
    mapAddr: null,
    BookingStatusInArray: {
      0: 'pending',
      1: 'confirmed',
      2: 'cancelled ',
      3: 'closed '
    },
  }
  constructor(
    public _api: ApiService,
    public _router: Router,
    public _ActivatedRouter: ActivatedRoute,
    public _toster:ToastrService
  ) { }

  ngOnInit() {
    this.OtherVariables.bookingID = this._ActivatedRouter.snapshot.paramMap.get('bookingID');
    this.OtherVariables.routerOutletId = this._ActivatedRouter.snapshot.paramMap.get('outletID');
    this.getBookingDetails();
  }

  getBookingDetails() {
    this.OtherVariables.loading = true
    let converterParams = {
      METHOD: 'GET',
      URL: environment.apiEndPoint + 'GetBooking',
      DATA: {
        bookingid: this.OtherVariables.bookingID,
        outletid: 'null',
        pageno: 0,
        maximumrows: 1,
        usepaging: 'false'
      }
    }
    this._api.getBookingDetails(converterParams).then((resp) => {
      if (resp && !resp.error && resp.hasOwnProperty('data') && resp.data.length && resp.data[0]) {
        resp.data[0].ReservationDate = moment(resp.data[0].ReservationDate).format('YYYY-MM-DD');
        this.OtherVariables.bookingDetails = resp.data[0];
        this.OtherVariables.mapAddr = 'https://maps.google.com/maps?q=' + resp.data[0].Address + ',' + resp.data[0].City + ',' + resp.data[0].State + '&t=&z=13&ie=UTF8&iwloc=&output=embed'
        console.log("RESPONSE", this.OtherVariables.bookingDetails)        
      }
      this.OtherVariables.loading = false;
    }).catch(err => {
      this.OtherVariables.loading = false;
      console.log("ERROR in Fertching details", err)
    });

  }

  gotoedit(){
    this._router.navigate(["booking/edit/"+this.OtherVariables.bookingID])
  }

  cancelBooking(data){
    data.ReservationDate = moment(data.ReservationDate).format('YYYY-MM-DD');
    console.log(data);
    if(confirm("Are you sure to cancel this reservation?")){
      var d = new Date();
      var datestring = d.getFullYear()  + "-" + (d.getMonth()+1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes();
      let converterParams = {
        METHOD: 'POST',
        URL: environment.apiEndPoint + '/BookingStatusChange',
        DATA: {
          BookingID: this.OtherVariables.bookingID,
          Status: 2,
          CancelReason: "",
          CancelationDate: datestring
        }
      }
      this._api.getBookingDetails(converterParams).then((resp) => {
        if(resp.data.Successful){
          this._toster.success("Your reservation successfully cancelled");
          this.getBookingDetails();
          //sms
          let OMN = '';
          let converterParamsN = {
              METHOD: 'GET',
              URL: environment.apiEndPoint + 'GetManagers',
              DATA: {
                  OutletID: this.OtherVariables.routerOutletId || 1
              }
          }
          this._api.getOutletDetails(converterParamsN).then((resp) => {
            if (resp.data.length > 0) {
                resp.data.forEach(element => {
                    OMN = OMN + element.CountryCode + element.PhoneNumber + ',';
                });
            }
            OMN = OMN.substring(0, OMN.length - 1);
            let msg = "Stickies Reservation Cancelled - "+data.OutletName+", " + data.FirstName + " "+ data.LastName + ", " + data.UserPhoneNumber + ", " + data.HeadCount + " pax, " + data.ReservationDate + " " + data.ReservationTime; 
            console.log('OMN=>',OMN);
            let converterParamsNN = {
                phonenos : OMN,
                msg: msg
            }
            this._api.sendsms(converterParamsNN).then((resp) => {
                console.log("resp==>",resp);
            }).catch(err => {
                console.log("ERROR in Fertching details", err);
            });
        }).catch(err => {
            console.log("ERROR in Fertching details", err);
        });
        }        
      }).catch(err => {
        this.OtherVariables.loading = false;
        console.log("ERROR in Fertching details", err)
      });
    }
  }

}
