import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { ApiService } from '../../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { ToastrService } from 'ngx-toastr';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';
declare var M;

@Component({
    selector: 'app-booking',
    templateUrl: './booking.component.html',
    styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {

    public restaurantCollapse: boolean = false;
    @ViewChild('datePick') datePick: any;
    public bookingForm: FormGroup;
    private routerOutletId: any;
    public outletDetails: any;
    public bookingDetails: any;
    public submitted: boolean = false;
    public callBookingAvailableApi: any = null;
    public callBookingAvailableApiResp: boolean = false;
    public populateTimeframe: any = [];
    public OtherVariables = {
        loading: true,
        btnloadder: false,
        ReservationCloseTime: "11:59 PM",
        ReservationOpenTime: "00:01 AM",
        isOutletOpen: true,
        headerText: '',
        TermsAndConditions: ''
    }
    public phoneNumber = "^(\+\d{1,3}[- ]?)?\d{10}$";
    public staticDate = moment().subtract(1, "days").format("YYYY-MM-DD");

    constructor(
        private _fb: FormBuilder,
        public callApi: ApiService,
        public route: ActivatedRoute,
        public router: Router,
        private toastr: ToastrService
    ) {
        // this.routerOutletId = this.route.snapshot.paramMap.get('OutletId');
        this.route.queryParams.subscribe(queryParams => {
            this.routerOutletId = queryParams['qr'];
        });
        this.bookingForm = this._fb.group({
            BookingID: new FormControl(''),
            OutletID: new FormControl(''),
            ReservationDate: new FormControl('', Validators.required),
            HeadCount: new FormControl('', Validators.required),
            ReservationTime: new FormControl('', Validators.required),
            Title: new FormControl('Mr.'),
            FirstName: new FormControl('', Validators.required),
            LastName: new FormControl('', Validators.required),
            Email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
            CountryCode: new FormControl('+65'),
            PhoneNumber: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]\\d{9}')])),
            SpecialNotes: new FormControl('')
        })
    }

    ngOnInit() {
        this.getOutletDetails();
        this.getOutletSettings();
    }

    ngAfterViewInit() {
        // let options = {};
        /* if(this.datePick.nativeElement) {
            var instances = M.Datepicker.init(this.datePick.nativeElement, '');
        } */

    }

    createTimeArr(startTime: string = '', endTime: string = '', timeInterVal: number = 15) {

        let current: any = new Date('2019/06/12 ' + startTime);//moment( '00:00' ).format('s');
        current = current.getTime();
        let end: any = new Date('2019/06/12 ' + endTime);
        end = end.getTime();
        let timeArr = [];
        timeInterVal = timeInterVal * 60 * 1000;

        for (let i = current; i <= end; i = i + timeInterVal) {
            let time = moment(i).format('hh:mm A');
            let text = moment(i).format('hh:mm A');
            timeArr.push({ value: time, text: text });
        }
        console.log("this.populateTimeframe", this.populateTimeframe)
        this.populateTimeframe = timeArr;
    }

    formatDateForPick() {
        let date = moment(this.bookingForm.value.ReservationDate).format('YYYY-MM-DD');
        this.bookingForm.patchValue({
            ReservationDate : date
        })
        console.log("this.bookingForm.value.ReservationDate",this.bookingForm.value.ReservationDate)
        this.getAvailability();
        this.getTimeFrame();
    }

    getTimeFrame(callFromDetails: boolean = false) {

        if (!callFromDetails) {
            this.bookingForm.patchValue({
                ReservationTime: ''
            })
            this.callBookingAvailableApi = null;
            this.callBookingAvailableApiResp = false;
        }
        if (this.bookingForm.value.ReservationDate) {
            console.log("DATETETETE",this.bookingForm.value.ReservationDate)
            let converterParams = {
                METHOD: 'GET',
                URL: environment.apiEndPoint + 'GetOutletSettings',
                DATA: {
                    OutletID: this.routerOutletId
                }
            }

            this.callApi.GetOutletSettings(converterParams).then((resp) => {
                if (resp && !resp.error && resp.data && !resp.data.hasOwnProperty('Key') && !resp.data.hasOwnProperty('Message')) {
                    let listTiems = resp.data.OutletSettingsDetailsList;
                    let current_day = moment(this.bookingForm.value.ReservationDate).day() + 1;
                    let intervalTime = resp.data.TimeInterval;
                    this.OtherVariables.headerText = resp.data.HeaderText;
                    this.OtherVariables.TermsAndConditions = resp.data.TermsAndConditions;

                    this.filetrTimeFrame(listTiems, current_day, intervalTime)
                }
            }).catch(err => {
                console.log("ERROR in Fertching details", err)
            });
        }
    }

    filetrTimeFrame(listTiems, current_day, intervalTime) {
        if (Array.isArray(listTiems) && listTiems.length) {
            let currentDateArr = listTiems.filter((arr) => {
                return arr.Day == current_day;
            })
            console.log("currentDateArr", currentDateArr)
            if (Array.isArray(currentDateArr) && currentDateArr.length && currentDateArr[0].EndTime) {
                let StartTime = currentDateArr[0].StartTime;
                let EndTime = currentDateArr[0].EndTime;
                this.createTimeArr(StartTime, EndTime, intervalTime)
            }
        }
    }

    getOutletDetails() {
        this.OtherVariables.loading = true;
        let converterParams = {
            METHOD: 'GET',
            URL: environment.apiEndPoint + 'GetAllOutlets',
            DATA: {
                BusinessUnitID: '',
                OutletID: this.routerOutletId || 1
            }
        }
        this.callApi.getOutletDetails(converterParams).then((resp) => {

            if (resp && !resp.error && resp.hasOwnProperty('data') && resp.data.length && resp.data[0]) {
                this.outletDetails = resp.data[0];
                console.log("RESPONSE", this.outletDetails)
                // this.toastr.success('Hello world!', 'Toastr fun!');
            }
            this.OtherVariables.loading = false;
            if (this.datePick) {
                this.datePick.nativeElement.setAttribute('min', moment().format('YYYY-MM-DD'));
            }

        }).catch(err => {
            console.log("ERROR in Fertching details", err);
            this.OtherVariables.loading = false;
        });

    }

    bookReservation(form) {
        //console.log("form==>",form);
        this.submitted = true;

        if (form.valid) {
            this.OtherVariables.btnloadder = true;
            let formData = form.value;
            formData.CurrentDateTime = moment().format("YYYY-MM-DD H:m:s");
            formData.OutletID = this.routerOutletId;

            console.log(formData);
            let converterParams = {
                METHOD: 'POST',
                URL: environment.apiEndPoint + 'InsertUpdateBookings',
                DATA: formData
            }

            this.callApi.insertUpdateBooking(converterParams).then((resp) => {
                if (resp && !resp.error && resp.data && !resp.data.hasOwnProperty("Key")) {
                    this.submitted = false;
                    this.bookingForm.patchValue({
                        BookingID: '',
                        OutletID: '',
                        ReservationDate: '',
                        HeadCount: '',
                        ReservationTime: '',
                        Title: 'Mr.',
                        CountryCode: '+65',
                        PhoneNumber: '',
                        SpecialNotes: ''
                    })
                    this.toastr.success("Booking Done Successfuly!");

                    //sms
                    let OMN = '';
                    let converterParamsN = {
                        METHOD: 'GET',
                        URL: environment.apiEndPoint + 'GetManagers',
                        DATA: {
                            OutletID: this.routerOutletId || 1
                        }
                    }

                    this.callApi.getOutletDetails(converterParamsN).then((resp) => {
                        if (resp.data.length > 0) {
                            resp.data.forEach(element => {
                                OMN = OMN + element.CountryCode + element.PhoneNumber + ',';
                            });
                        }
                        OMN = OMN.substring(0, OMN.length - 1);
                        let msg = "Stickies Reservation - " + this.outletDetails.OutletName + ", " + formData.FirstName + " " + formData.LastName + ", " + formData.PhoneNumber + ", " + formData.HeadCount + " pax, " + formData.ReservationDate + " " + formData.ReservationTime;
                        console.log('OMN=>', OMN);
                        let converterParamsNN = {
                            phonenos: OMN,
                            msg: msg
                        }
                        this.callApi.sendsms(converterParamsNN).then((resp) => {
                            console.log("resp==>", resp);
                        }).catch(err => {
                            console.log("ERROR in Fertching details", err);
                        });
                    }).catch(err => {
                        console.log("ERROR in Fertching details", err);
                    });
                    this.OtherVariables.btnloadder = false;
                    this.router.navigate(['booking-details/' + resp.data.ReturnGUID]);
                } else {
                    this.OtherVariables.btnloadder = false;
                    this.toastr.error("Error in booking!");
                }
            }).catch(err => {
                this.OtherVariables.btnloadder = false;
                console.log("ERROR in Fertching details", err)
            });
        } else {
            this.OtherVariables.btnloadder = false;
            this.toastr.warning("Action Required!");
        }


    }

    getAvailability() {
        if (this.bookingForm.value.ReservationDate && this.bookingForm.value.HeadCount && this.bookingForm.value.ReservationTime) {
            this.callBookingAvailableApi = true;
            let converterParams = {
                METHOD: 'GET',
                URL: environment.apiEndPoint + 'BookingAvailable',
                DATA: {
                    HeadCount: this.bookingForm.value.HeadCount,
                    OutletID: this.routerOutletId,
                    ReservationDate: this.bookingForm.value.ReservationDate,
                    CurrentDateTime: moment().format("YYYY-MM-DD H:m:s"),
                    ReservationTime: this.bookingForm.value.ReservationTime
                }
            }

            this.callApi.bookingAvailability(converterParams).then((resp) => {
                this.callBookingAvailableApi = false;
                if (resp && !resp.error) {
                    this.callBookingAvailableApiResp = resp.data;
                    console.log("RESPONSE", resp)
                }
            }).catch(err => {
                console.log("ERROR in Fertching details", err)
            });
        }
    }

    getOutletSettings() {
        let converterParams = {
            METHOD: 'GET',
            URL: environment.apiEndPoint + 'GetOutletSettings',
            DATA: {
                OutletID: this.routerOutletId
            }
        }
        this.callApi.GetOutletSettings(converterParams).then((resp) => {
            if (resp && !resp.error && resp.data && !resp.data.hasOwnProperty('Key') && !resp.data.hasOwnProperty('Message')) {
                this.OtherVariables.ReservationCloseTime = resp.data.ReservationCloseTime;
                this.OtherVariables.ReservationOpenTime = resp.data.ReservationOpenTime;
                this.OtherVariables.headerText = resp.data.HeaderText;
                this.OtherVariables.TermsAndConditions = resp.data.TermsAndConditions;
                var format = 'hh:mm A';
                var time = moment();
                var beforeTime = moment(resp.data.ReservationOpenTime, format);
                var afterTime = moment(resp.data.ReservationCloseTime, format);
                if (time.isBetween(beforeTime, afterTime)) {
                    this.OtherVariables.isOutletOpen = true;
                } else {
                    this.OtherVariables.isOutletOpen = false;
                }
            }
        }).catch(err => {
            console.log("ERROR in Fertching details", err)
        });
    }
}
