import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookingComponent } from './pages/booking/booking.component';
import { OutletlistComponent } from './pages/outletlist/outletlist.component';
import { OutletaboutComponent } from './pages/outletabout/outletabout.component';
import { EditbookingComponent } from './pages/editbooking/editbooking.component';
import { BookingDetailsComponent } from './pages/booking-details/booking-details.component';
import { CancelbookingComponent } from './pages/cancelbooking/cancelbooking.component';

const routes: Routes = [
  {path: '', component: OutletlistComponent},
  {path: 'outlet/about/:id', component: OutletaboutComponent},
  {path: 'booking', component: BookingComponent },
  {path: 'booking/edit/:bookingID', component: EditbookingComponent },
  {path: 'booking-details/:bookingID', component: BookingDetailsComponent },
  {path: 'booking/cancel/:bookingID', component: CancelbookingComponent },
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
